---
title: moxa-mxu11x0-driver
---

## Moxa UPort 11x0 USB to Serial Hub Linux driver
The MOXA UPort 11x0 USB to Serial Hub Linux driver supports following devices.

- UPort 1110, 1 port RS-232 USB to Serial Hub.
- UPort 1130, 1 port RS-422/485 USB to Serial Hub.
- UPort 1130I, 1 port RS-422/485 USB to Serial Hub with isolation
  protection.
- UPort 1150, 1 port RS-232/422/485 USB to Serial Hub.
- UPort 1150I, 1 port RS-232/422/485 USB to Serial Hub with isolation
  protection.
- USB Console, 1 port RS-232 USB to Serial Hub.
- USB-to-Serial Port Driver, 1 port RS-232/422/485 USB to Serial Hub

### Support Models

- DA Series:
  - [DA-680](../products/DA/DA-680.md)

### Source Code Link
- For Kernel Version 5.x
  - [moxa-mxu11x0-driver (5.x)](https://gitlab.com/moxa/ibg/software/platform/linux/packages/moxa-mxu11x0-driver/-/tree/5.x/5.1.7-23010918/master)

- For Kernel Version 3.x
  - [moxa-mxu11x0-driver (3.x)](https://gitlab.com/moxa/ibg/software/platform/linux/packages/moxa-mxu11x0-driver/-/tree/3.x/3.0.1-23020203/master)

:::caution
Now the driver is beta version.
:::

### Build kernel module
- DA-680
```
$ ./mxinstall install kflags="-DDEFAULT_UART_MODE=0 -DNOT_ALLOW_RS232 -DNOT_ALLOW_RS422 -DNOT_ALLOW_RS4854W"
```

### What Tools Depend On?

- [mx-uart-ctl](../tools/mx-uart-ctl.md)
