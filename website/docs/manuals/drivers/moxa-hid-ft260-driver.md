---
title: moxa-hid-ft260-driver
---

### Moxa HID FT260 driver

Derived from Linux kernel upstream: [drivers/hid/hid-ft260.c](https://github.com/torvalds/linux/commits/master/drivers/hid/hid-ft260.c)

#### Support Models

- DA Series: [DA-820C](../products/DA/DA-820C.md)|[DA-682C](../products/DA/DA-682C.md)|[DA-681C](../products/DA/DA-681C.md)

#### Source Code Link

- [moxa-hid-ft260-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-hid-ft260-driver/-/tree/5.15/master)

### What Tools Depend On?

- [mx-uart-ctl](../tools/mx-uart-ctl.md)

### Related Drivers

- [moxa-gpio-pca953x-driver](../drivers/moxa-gpio-pca953x-driver.md)

### Add Udev Rules to Rebind FT260 Device

- To avoid the ft260 hid device is pre-bind to hid-generic subsystem, add udev rules to re-bind to ft260 driver.
- Edit `/etc/udev/rules.d/11-ft260-pca9535.rules`
```
ACTION=="add", KERNEL=="0003:0403:6030.*", SUBSYSTEM=="hid", DRIVERS=="hid-generic", \
RUN+="/bin/bash -c 'echo $kernel > /sys/bus/hid/drivers/hid-generic/unbind'", \
RUN+="/bin/bash -c 'echo $kernel > /sys/bus/hid/drivers/ft260/bind'"
```

### Usage (Debian 11/Ubuntu 20.04)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `hid-ft260.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Sync the latest version available from any enabled repository and reboot system

```
yum distro-sync
reboot
```

- Install required packages

```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum groupinstall "Development Tools"
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `hid-ft260.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::