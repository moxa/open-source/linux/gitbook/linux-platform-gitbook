---
title: moxa-irigb-driver
---

## Moxa IRIG-B Driver

The IRIG-B driver is for Moxa embedded compute for controlling the IRIG-B device.

### Support Models

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-720](../products/DA/DA-720.md)
  - [DA-680](../products/DA/DA-680.md)

### Source Code Link

[moxa-irigb-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-irigb-driver/-/tree/master)

### What Tools Depend On?

[moxa-irigb-tools](../tools/moxa-irigb-tools.md)

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update && apt install build-essential
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `moxa_irigb.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Sync the latest version available from any enabled repository and reboot system

```
yum distro-sync
reboot
```

- Install required packages

```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum groupinstall "Development Tools"
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `moxa_irigb.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::