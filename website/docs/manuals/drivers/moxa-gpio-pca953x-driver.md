---
title: moxa-gpio-pca953x-driver
---

### Moxa gpio pca953x driver

`moxa-gpio-pca953x-driver`: Derived from drivers/i2c/chips/pca9539.c

Clone ource from: https://github.com/torvalds/linux/blob/master/drivers/gpio/gpio-pca953x.c

#### Support Models

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-682C](../products/DA/DA-682C.md)
  - [DA-681C](../products/DA/DA-681C.md)
- V Series
  - [V3000](../products/V/V3000.md)
- EXPC Series
  - [EXPC-F2000](../products/EXPC/EXPC-F2000.md)

#### Source Code Link

- For Kernel Version = 3.x 
  - [moxa-gpio-pca953x-driver (3.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-gpio-pca953x-driver/-/blob/3.19/master/gpio-pca953x.c)

- For Kernel Version = 4.x
  - [moxa-gpio-pca953x-driver (4.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-gpio-pca953x-driver/-/blob/4.19/master/gpio-pca953x.c)

- For Kernel Version = 5.x
  - [moxa-gpio-pca953x-driver (5.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-gpio-pca953x-driver/-/blob/5.10/master/gpio-pca953x.c)


### What Tools Depend On?

- [mx-uart-ctl](../tools/mx-uart-ctl.md)
- [mx-module-ctl](../tools/mx-module-ctl.md)
- [mx-m2b-module-ctl](../tools/mx-m2b-module-ctl.md)


### Usage (Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-pca953x.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (Ubuntu 20.04)

`gpio-pca953x.ko` driver is default pre-installed in `linux-modules-$(uname -r)` package

### Usage (CentOS 7.9)

- Sync the latest version available from any enabled repository and reboot system
```
yum distro-sync
reboot
```

- Install required packages
```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum groupinstall "Development Tools"
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-pca953x.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::
