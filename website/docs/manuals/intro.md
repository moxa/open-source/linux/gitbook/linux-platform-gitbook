---
title: Introduction
---

The following tables provide the relationship with models, including supported drivers, tools and modules.

### Drivers Support

List of Drivers:
- Super IO drivers:
  - [moxa-it87-gpio-driver](drivers/moxa-it87-gpio-driver.md)
  - [moxa-it87-serial-driver](drivers/moxa-it87-serial-driver.md)
  - [moxa-it87-wdt-driver](drivers/moxa-it87-wdt-driver.md)
- GPIO expander drivers:
  - [moxa-gpio-pca953x-driver](drivers/moxa-gpio-pca953x-driver.md)
- HID device drivers:
  - [moxa-hid-ft260-driver](drivers/moxa-hid-ft260-driver.md)
- Expansion modules' drivers:
  - [moxa-irigb-driver](drivers/moxa-irigb-driver.md)
- Moxa misc drivers:
  - [moxa-misc-driver](drivers/moxa-misc-driver.md)
- Moxa hotswap driver:
  - [moxa-hotswap-driver](drivers/moxa-hotswap-driver.md)
- Moxa igc driver:
  - [moxa-igc-driver](drivers/moxa-igc-driver.md)
- Moxa UPort driver:
  - [moxa-mxu11x0-driver](drivers/moxa-mxu11x0-driver.md)

| Models\Drivers | it87-gpio          | it87-serial        | it87-wdt           | gpio-pca953x       | hid-ft260          | moxa-irigb         | moxa-misc          | moxa-hotswap       | igc                | mxu11x0            |
| -------------- | -------------------| ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ |
| DA-820C        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |
| DA-682C        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |
| DA-681C        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |
| MC-3201        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |
| DA-720         |                    |                    | :heavy_check_mark: |                    |                    | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |
| MPC-2070/2120  |                    |                    | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: |                    |                    |                    |
| V2403C         | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |
| V2406C         | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    | :heavy_check_mark: |                    |                    |
| V2201          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |
| V3000          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    | :heavy_check_mark: |                    |
| EXPC-F2000     |                    |                    | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |
| MC-1100        |                    |                    | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: |                    |                    |                    |
| MC-1200        | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |
| MC-7400        |                    |                    | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: |                    |                    |                    |
| DA-680         | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: |

### Tools Support

List of Tools:
- IO peripherals:
  - [mx-uart-ctl](tools/mx-uart-ctl.md)
  - [mx-dio-ctl](tools/mx-dio-ctl.md)
  - [mx-led-ctl](tools/mx-led-ctl.md)
  - [mx-relay-ctl](tools/mx-relay-ctl.md)
  - [mx-power-igtd](tools/mx-power-igtd.md)
  - [moxa-audio-retask](tools/moxa-audio-retask.md)
  - [moxa-drive-hotswapd](tools/moxa-drive-hotswapd.md)
- Expansion modules' tools:
  - [moxa-hsrprp-tools](tools/moxa-hsrprp-tools.md)
  - [moxa-irigb-tools](tools/moxa-irigb-tools.md)

| Models\Tools     | mx-uart-ctl        | mx-dio-ctl         | mx-led-ctl         | mx-relay-ctl       | moxa-hsrprp-tools  | moxa-irigb-tools   | mx-power-igtd      | moxa-audio-retask  | moxa-drive-hotswapd |
| ---------------- | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ |------------------- | ------------------ |-------------------- |
| DA-820C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                     |
| DA-682C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                     |
| DA-681C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                     |
| DA-720           |                    |                    |                    |                    | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                     |
| V2403C           | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: | :heavy_check_mark: |                     |
| V2406C           | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    | :heavy_check_mark: | :heavy_check_mark:  |
| V2201            | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |                     |
| V3000            | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    |                    |                    |                    |                    |
| EXPC-F2000       | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |                     |
| MC-1200          | :heavy_check_mark: |                    |                    |                    |                    |                    |                    |                    |                     |
| MC-7400          |                    |                    |                    |                    |                    |                    |                    | :heavy_check_mark: |                     |
| DA-680           | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    |                    |                     |

### Cellular Module Control Tools Support

List of Cellular Module Control Tools:
- For mPCIe form factor: [mx-module-ctl](tools/mx-module-ctl.md)
- For m.2 B key form factor: [mx-m2b-module-ctl](tools/mx-m2b-module-ctl.md)

| Models\Tools     |    mx-module-ctl   | mx-m2b-module-ctl  |
| ---------------- | ------------------ | ------------------ |
| V2201            | :heavy_check_mark: |                    |
| V2403C           | :heavy_check_mark: |                    |
| V2406C           | :heavy_check_mark: |                    |
| V3000            | :heavy_check_mark: | :heavy_check_mark: |

### MCU Control Tools Support

List of MCU Control Tools:
- MCU APP Watchdog: [moxa-app-wdt-utility](tools/moxa-app-wdt-utility.md)
- MCU LAN bypass Utils: [moxa-lan-bypass-utility](tools/moxa-lan-bypass-utility.md)
- MCU Upgrade Tool: [moxa-lpc-mcu-upgrade-tool](tools/moxa-lpc-mcu-upgrade-tool.md)
- MCU Scaler Utils: [moxa-scaler-utils](tools/moxa-scaler-utils.md)
- MCU LED Control Tool: [mx-expc-led-ctl](tools/mx-expc-led-ctl.md)

| Models\Tools     | MCU APP Watchdog   |  MCU LAN bypass Utils | MCU Upgrade Tool  | MCU LED Control Tool | MCU Scaler Utils   |
| ---------------- | ------------------ |  -------------------- |------------------ | -------------------- | ------------------ |
| EXPC-F2000       |                    |                       |:heavy_check_mark: | :heavy_check_mark:   | :heavy_check_mark: |
| V3000            | :heavy_check_mark: |  :heavy_check_mark:   |:heavy_check_mark: |                      |                    |

### Expansion Module Cards Support

List of **DA/DN** Expansion Modules:
- PRP-HSR-I210 Expansion Modules: [DA/DN-PRP-HSR-I210](others/expansion_modules/DA_DN-PRP-HSR-I210.md)
- UART Expansion Modules: [DN-SP08-I-DB/TB](others/expansion_modules/DN-SP08-I-DB_TB.md)
- IRIGB Expansion Modules: [DA-IRIGB-B-S](others/expansion_modules/IRIGB.md)

| Models\Tools     |   DA-PRP-HSR-I210  |  DN-PRP-HSR-I210   |  DN-SP08-I-DB/TB   |   DA-IRIGB-B-S     |
| ---------------- | ------------------ | ------------------ | ------------------ | ------------------ |
| DA-820C          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| DA-682C          |                    | :heavy_check_mark: | :heavy_check_mark: |                    |

List of **DE** Expansion Modules:
- DE-PRP-HSR-EF Expansion Modules: [DE-PRP-HSR-EF](others/expansion_modules/DA_DN-PRP-HSR-I210.md)
- UART Expansion Modules: [DE-SP08-I-TB](others/expansion_modules/DE-SP08-I-TB.md)
- IRIGB Expansion Modules: [DE-2-IRIGB-4-DI/DO](others/expansion_modules/IRIGB.md)

| Models\Tools     |   DE-PRP-HSR-EF    | DE-2-IRIGB-4-DI/DO |  DE-SP08-I-TB      |
| ---------------- | ------------------ | ------------------ | ------------------ |
| DA-720           | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
