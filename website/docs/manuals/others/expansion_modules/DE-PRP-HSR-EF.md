---
title: DE-PRP-HSR-EF
---

## Datasheet

### DE-PRP-HSR-EF

DA-720-Ethernet peripheral expansion modules include a 4-port Giga LAN module, an 8-port Giga LAN module, and a PRP/HSR module for setting
up industrial communication applications with Ethernet-based devices. All modules are designed to offer the greatest flexibility for setting up
applications and performing industrial tasks.

The PRP-HSR expansion module is compliant with IEC 62439-3 Clause 4 (PRP) and IEC 62439-3 Clause 5 (HSR) to ensure the highest system
availability and data integrity for mission-critical applications that require redundancy and zero-time recovery. With dual Gigabit Ethernet port
design, the DA-PRP-HSR module provides high performance for redundant network systems. Moxa’s DA-720 Series industrial computer with the
DA-PRP-HSR module installed is the ideal solution for power substation automation and process automation systems.

- [DE-PRP-HSR-EF Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-720-ethernet-series-expansion-modules/de-prp-hsr-ef)
- [moxa-da-720-ethernet-series-expansion-modules-datasheet-v1.0.pdf](https://www.moxa.com/getmedia/fac70504-2932-457d-8612-7f809cbdde69/moxa-da-720-ethernet-series-expansion-modules-datasheet-v1.0.pdf)

## Prerequisite

- See [Introduction](../../intro.md) page for more informations.
- See [moxa-hsrprp-tools](../../tools/moxa-hsrprp-tools.md) for Moxa HSR/PRP card utility.

## Support Model

- DA Series:
  - [DA-720](../../products/DA/DA-720.md)
