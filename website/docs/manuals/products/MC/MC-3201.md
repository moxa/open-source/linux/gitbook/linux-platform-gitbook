---
title: MC-3201
---

### Tech Specs
| Computer     | <!-- -->                                                             |
|--------------|----------------------------------------------------------------------|
| CPU          | Tiger Lake CPU main Board (Celeron/i3/i5/i7)                         |
| Storage Slot | Hot-swappable 2.5” SSD/HDD slot x 1                                  |
| Supported OS | Windows 10 IoT 2019 LTSC/Debian-11 (bullseye)/Ubuntu-20.04-HWE       |
| Memory Slot  | DDR4 SO-DIMM slot x 2 (Up to 32GB)                                   |

| Peripherals          | <!-- -->                                                          |
|----------------------|-------------------------------------------------------------------|
| Ethernet Ports       | Auto Sensing 10/100/1000Mbps x 4 (I219 x 1 + I210 x 3)            |
| Serial Ports         | RS-232/422/485 x 2, software selectable (DB9)                     |
| Digital Input/Output | DI x 4, DO x 4 (GPIO)                                             |
| NMEA                 | FT4232 (UART X 4)                                                 |
| USB                  | USB 2.0 Port x 4 (type-A)/USB 3.0 Port x 2 (type-A)               |
| Video Output         | DisplayPort x 2                                                   |
| Audio Input/Output   | Line in x 1, Line out x 1, 3.5 mm phone jack                      |
| M.2 Socket           | Cellular 5G module/Storage x 1 (B key)/WiFi module x 1 (E key)    |
| SIM Card Socket      | micro SIM socket x 4 (push-out type)                              |
| mini PCIe Socket     | Cellular LTE module x 1                                           |

### Models
| <!-- -->      | MC-3201-TL1-M-S      | MC-3201-TL7-S-S     |
|---------------|----------------------|---------------------|
| Processor     | Intel Corei7-1185G7E | Intel Celeron 3965U |
| RAM           | N/A                  | N/A                 |
| HDD/SSD Slots | N/A                  | N/A                 |
| LAN Ports     | N/A                  | N/A                 |
| Serial Ports  | N/A                  | N/A                 |
| DI/DO         | N/A                  | N/A                 |
| NMEA 0183     | N/A                  | N/A                 |
| Power Input   | N/A                  | N/A                 |

### Control Peripherals
#### Prerequisite
Build necessary MOXA out-of-tree kernel module for controlling **Super I/O**.
- Install kernel module tool.
  ```bash
  sudo apt install build-essential kmod
  ```
- Install kernel headers.
  ```bash
  sudo apt update
  sudo apt install linux-headers-`uname -r`
  ```
- Build necessary kernel modules.
  - [moxa-it87-serial-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-serial-driver/tree/master)
  - [moxa-it87-gpio-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-gpio-driver/tree/master)
  - [moxa-it87-wdt-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver)
    - For Ubuntu 20.04/Debian 11 (kernel ver >= 5.2): [moxa-it87-wdt-driver(5.2)](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/tree/5.2/master)
- Install and generate modules.dep.
    ```bash
    make && make install
    depmod -a
    ```
- Example Code for set/get Super IO GPIO value
  - [mx-gpio-ctl](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/GPIO/mx-gpio-ctl)
  ```
  wget https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/raw/main/x86/GPIO/mx-gpio-ctl
  chmod +x mx-gpio-ctl
  cp mx-gpio-ctl /usr/sbin/
  ```

---

#### UART
UART modes are controled by 3 gpio pins and it87_serial
- Super IO (IT8786) PIN table

| Device Node | GPIO#0 | GPIO#1 | GPIO#2 | it87_serial |
| ----------  | ------ | ------ | ------ | ----------- |
| /dev/ttyS0  |  GP13  |  GP11  |  GP12  |      1      |
| /dev/ttyS1  |  GP16  |  GP14  |  GP15  |      2      |

- GPIO value of UART modes

|   UART mode    | GPIO#0 | GPIO#1 | GPIO#2 | it87_serial |
| -------------- | ------ | ------ | ------ | ----------- |
| RS232          |   1    |    0   |    0   |      0      |
| RS485-2W       |   0    |    1   |    0   |      1      |
| RS422/RS485-4W |   0    |    0   |    1   |      1      |

- Example for port 1
  - Select port 1 (/dev/ttyS0) as **RS232** mode
    ```bash
    mx-gpio-ctl set 13 1
    mx-gpio-ctl set 11 0
    mx-gpio-ctl set 12 0
    echo 0 > /sys/class/misc/it87_serial/serial1/serial1_rs485
    ```
  - Select port 1 (/dev/ttyS0) as **RS485-2W** mode
    ```bash
    mx-gpio-ctl set 13 0
    mx-gpio-ctl set 11 1
    mx-gpio-ctl set 12 0
    echo 1 > /sys/class/misc/it87_serial/serial1/serial1_rs485
    ```
  - Select port 1 (/dev/ttyS0) as **RS422/RS485-4W** mode
    ```bash
    mx-gpio-ctl set 13 0
    mx-gpio-ctl set 11 0
    mx-gpio-ctl set 12 1
    echo 1 > /sys/class/misc/it87_serial/serial1/serial1_rs485
    ```
  - Get port 1 (/dev/ttyS0) mode
    ```bash
    mx-gpio-ctl get 13
    mx-gpio-ctl get 11
    mx-gpio-ctl get 12
    cat /sys/class/misc/it87_serial/serial1/serial1_rs485
    ```
  - Result:
    ```bash
    0
    1
    0
    1
    ```
    Then look up the table, port 1 is on **RS485-2W** mode.

- Example for port 2
  - Select port 2 (/dev/ttyS1) as **RS232** mode
    ```bash
    mx-gpio-ctl set 16 1
    mx-gpio-ctl set 14 0
    mx-gpio-ctl set 15 0
    echo 0 > /sys/class/misc/it87_serial/serial2/serial2_rs485
    ```
  - Select port 2 (/dev/ttyS1) as **RS485-2W** mode
    ```bash
    mx-gpio-ctl set 16 0
    mx-gpio-ctl set 14 1
    mx-gpio-ctl set 15 0
    echo 1 > /sys/class/misc/it87_serial/serial2/serial2_rs485
    ```
  - Select port 2 (/dev/ttyS1) as **RS422/RS485-4W** mode
    ```bash
    mx-gpio-ctl set 16 0
    mx-gpio-ctl set 14 0
    mx-gpio-ctl set 15 1
    echo 1 > /sys/class/misc/it87_serial/serial2/serial2_rs485
    ```
  - Get port 2 (/dev/ttyS1) mode
    ```bash
    mx-gpio-ctl get 16
    mx-gpio-ctl get 14
    mx-gpio-ctl get 15
    cat /sys/class/misc/it87_serial/serial2/serial2_rs485
    ```
  - Result:
    ```text
    0
    1
    0
    1
    ```
    Then look up the table, port 2 is on **RS485-2W** mode.
- Use helper script to swtich UART mode.
  - Read GPIO table(**Config/mc3201-uart-gpio**) in **ExampleCode/mx-uart-mode** script.
    ```bash
    source ./mc3201-uart-gpio
    ```
  - Select port 1 (/dev/ttyS0) as RS232 mode.
    ```bash
    ./mx-uart-mode set 1 1
    Set UART port 1 to RS232 mode
    ```
  - Get port 1 (/dev/ttyS0) mode.
    ```bash
    ./mx-uart-mode get 1
    RS232
    ```
---

#### NMEA
- FT4232 UART chip

| Port |  Device Node  |
| ---- | ------------  |
|  1   | /dev/ttyUSB0  |
|  2   | /dev/ttyUSB1  |
|  3   | /dev/ttyUSB2  |
|  4   | /dev/ttyUSB3  |

---

#### DIO
- DIO status will be kept when warn (soft) reboot
- DO = HIGH (Relay, Close)  -->  DI = LOW
- DO = LOW (Relay, Open) -->  DI = HIGH
- DI: please DO NOT set DI port value when using example code
- CP2112 PIN table

| DIO index   | GPIO PIN |
| ----------  | -------- |
| DI 0        |    0     |
| DI 1        |    1     |
| DI 2        |    2     |
| DI 3        |    3     |
| DO 0        |    4     |
| DO 1        |    5     |
| DO 2        |    6     |
| DO 3        |    7     |

- Example
  - Get DI 0 state
    ```bash
    mx-cp2112-gpio-ctl get 0
    ```
  - Get DO 0 state
    ```bash
    mx-cp2112-gpio-ctl get 4
    ```
  - Set DO 0 state as low
    ```bash
    mx-cp2112-gpio-ctl set 4 0
    ```

- Notice
If DI port has been set value, the direction will set to "out".  
This will cause DI return value failed, please DO NOT set DI port value when using example code.  
But if DI port has been set value, please using the following step to reset dirction to 'in' status.  

```
# grep cp2112_gpio /sys/class/gpio/gpiochip*/label
/sys/class/gpio/gpiochip440/label:cp2112_gpio

# then the base number = 440
# DI GPIO numbers will be DI_0=440, DI_1=441, DI_2=442, DI_3=443
echo "in" > /sys/class/gpio/gpio440/direction
echo "in" > /sys/class/gpio/gpio441/direction
echo "in" > /sys/class/gpio/gpio442/direction
echo "in" > /sys/class/gpio/gpio443/direction
```

---

#### Cellular

mPCIe slot power control

- Default status is according to BIOS setting
- Power control:

| MPCIE PWR CTL |     GPIO PIN    |
| ------------  | --------------- |
|   SIO PIN     |       GP46      |

- Reset control:

| MPCIE RST CTL |     GPIO PIN    |
| ------------  | --------------- |
|   SIO PIN     |       GP41      |

- For power on mPCIe slot
  - Power ON -> wait 200 ms -> and RST ON
  - Set GP46 High, wait 200 ms, and Set GP41 High

- For power off mPCIe slot
  - RST OFF -> wait 200 ms -> and Power OFF
  - Set GP41 Low, wait 200 ms, and Set GP46 Low

- For reset module only
  - RST OFF -> RST ON
  - Set GP41 Low, wait 200 ms, and GP41 High

- Example
  - Power on mPCIe slot
    ```bash
    mx-gpio-ctl set 46 1; sleep 0.2; mx-gpio-ctl set 41 1
    ```
  - Power off mPCIe slot
    ```bash
    mx-gpio-ctl set 41 0; sleep 0.2; mx-gpio-ctl set 46 0
    ```
  - Reset on mPCIe slot
    ```bash
    mx-gpio-ctl set 41 0; sleep 0.2; mx-gpio-ctl set 41 1`
    ```
  - Get power status on slot
    ```bash
    mx-gpio-ctl get 41
    ```
  - Get reset pin status on slot
    ```bash
    mx-gpio-ctl get 46
    ```

- Notice
If SIM card slots have been changed, please **power cycle module** to let SIM cards could be re-detected.

#### M.2 Key B slot
N/A

#### SIM card select

| SIM SEL          | GPIO PIN | SLOT Type |
| ---------------  | -------- | --------- |
| SIM Slot #1/#2   |   GP80   | mPCIe     |
| SIM Slot #3/#4   |   GP82   | M.2 Key B |

- Default status
  - mPCIe: SIM slot #1 (Low)
  - M.2 Key B: SIM slot #3 (Low)

- Select SIM slot
  - mPCIe:
    - Set GP80 Low = Select SIM slot #1
    - Set GP80 High = Select SIM slot #2
  - M.2 Key B:
    - Set GP82 Low = Select SIM slot #3
    - Set GP82 High = Select SIM slot #4

- Example
  - Select SIM slot #1
    ```bash
    mx-gpio-ctl set 80 0
    ```
  - Select SIM slot #2
    ```bash
    mx-gpio-ctl set 80 1
    ```
  - Select SIM slot #3
    ```bash
    mx-gpio-ctl set 82 0
    ```
  - Select SIM slot #4
    ```bash
    mx-gpio-ctl set 82 1
    ```
  - Get SIM slot #1/#2 status
    ```bash
    mx-gpio-ctl get 80
    ```
  - Get SIM slot #3/#4 status
    ```bash
    mx-gpio-ctl get 82
    ```

- LTE Telit module dial-up and connection guide
TBD

---

#### LAN interface

| LAN Slot |  NIC |  renamed NIC  |
| -------- | ---- | ------------- |
| LAN #1   | eth0 |   enp0s31f6   |
| LAN #2   | eth1 |   enp6s0      |
| LAN #3   | eth2 |   enp7s0      |
| LAN #4   | eth3 |   enp8s0      |

- For Ubuntu 20.04 LAN setting:
I219 LAN chip: only support on **Ubuntu 20.04 HWE** kernel version:

```bash
sudo apt update
sudo apt install --install-recommends linux-generic-hwe-20.04
uname -a
Linux moxa 5.11.0-34-generic #36~20.04.1-Ubuntu SMP Fri Aug 27 08:06:32 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux

ip a | grep enp
2: enp6s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
3: enp7s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
4: enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    inet 10.123.8.84/23 brd 10.123.9.255 scope global enp8s0
5: enp0s31f6: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
```
- Ref
https://askubuntu.com/questions/1344156/ubuntu-20-04-2-and-onboard-intel-i219-v

---

#### Watchdog
- Example for probing `it87_wdt` driver on boot
  1. Config file `/lib/modprobe.d/watchdog.conf`
      ```text
      # timeout:Watchdog timeout in seconds, default=60 (int)
      # nowayout:Watchdog cannot be stopped once started, default=0 (bool)
      options it87_wdt nowayout=1 timeout=60
      ```

  2. Disable `iTCO_wdt` and `mei_wdt` driver, `/lib/modprobe.d/iTCO-blacklist.conf`
      ```text
      blacklist iTCO_wdt
      blacklist mei_wdt
      blacklist iTCO_vendor_support
      ```

  3. Disalbe `NMI watchdog` driver (for Ubuntu), `/etc/default/grub`
      ```text
      GRUB_CMDLINE_LINUX_DEFAULT="nmi_watchdog=0"
      ```
  4. After adding the kernel parameter, don't forget to update the GRUB
      ```bash
      sudo update-grub
      ```
- Watchdog daemon package (Debian/Ubuntu)
  1. Install package
      ```bash
      sudo apt-get update
      sudo apt-get install watchdog
      ```
  2. Config file `/etc/default/watchdog`
      ```text
      # Start watchdog at boot time? 0 or 1
      run_watchdog=1
      # Start wd_keepalive after stopping watchdog? 0 or 1
      run_wd_keepalive=0
      # Load module before starting watchdog
      watchdog_module="it87_wdt"
      # Specify additional watchdog options here (see manpage).
      ```
  3. To uncomment this to use the watchdog device driver access "file", `/etc/watchdog.conf`
      ```text
      watchdog-device                = /dev/watchdog
      ```

- Example
https://github.com/torvalds/linux/tree/master/tools/testing/selftests/watchdog
