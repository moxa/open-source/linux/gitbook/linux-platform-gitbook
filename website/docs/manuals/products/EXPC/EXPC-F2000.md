---
title: EXPC-F2000
---

### Datasheet
- TBD

### Prerequisite
See [Introduction](../../intro.md) page to check necessary drivers/tools/modules informations.

### Linux Distribution
- Ubuntu 22.04 LTS
- Debian 11

### Troubleshooting

1. Panel touchpad is not working when enter blank screen on Ubuntu GNOME desktop environment  

  If host enter the blank screen mode, the panel touchpad (as USB hid device) is not working somehow.  
  But user can move the mouse cursor or press the buttons on keyboard to resume the panel touchpad function,  
  it suggested that to disable blank screen mode as 'Never' when using panel touchpad only.  
  
  To disable blank screen mode on Ubuntu GNOME desktop:  
  **System Settings > Power > Power Saving Options > Screen Blank > Select 'Never' Option**
