---
title: V2403C
---

### Datasheet
- [V2403C Series Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/v2403c-series)
- [moxa-v2403c-series-datasheet-v2.0.pdf](https://cdn-cms.azureedge.net/getmedia/1240a288-596b-4c58-b597-495ecbe7058d/moxa-v2403c-series-datasheet-v2.0.pdf)

### Prerequisite
See [Introduction](../../intro.md) page to check necessary drivers/tools/modules informations.