---
title: moxa-scaler-utils
---

### Moxa scaler utility

The `Moxa scaler utility` provides utility and daemon to control MCU scaler,
including the light-sensor, brightness, touch-button functions for panel.

### Source Code Link
[moxa-scaler-utils](https://gitlab.com/moxa/open-source/linux/packages/moxa-scaler-utils/-/tree/master)

### Support Model

- EXPC Series:
  - [EXPC-F2000](../products/EXPC/EXPC-F2000.md)

### Prerequisite
- install necessary libary and build tools
```
apt update && apt install git build-essential libjson-c-dev -y
```

- build code, and install scaler daemon and utility
```
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-scaler-utils.git -b master
cd moxa-scaler-utils
make
make install
```

- start systemd service
```
cp moxa-scalerd.service /lib/systemd/system/
systemctl enable moxa-scalerd.service

# start and check service status
systemctl start moxa-scalerd.service
systemctl status moxa-scalerd.service
```

the moxa-scalerd service status shall be like:
```
● moxa-scalerd.service - Moxa scaler daemon service
     Loaded: loaded (/lib/systemd/system/moxa-scalerd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-06-28 02:28:17 EDT; 15min ago
   Main PID: 15525 (moxa-scalerd)
      Tasks: 2 (limit: 18923)
     Memory: 208.0K
        CPU: 4.163s
     CGroup: /system.slice/moxa-scalerd.service
             └─15525 /usr/sbin/moxa-scalerd

Jun 28 02:28:17 debian systemd[1]: Started Moxa scaler daemon service.
```

- scaler utility

For `EXPC-F2000` series config:
```
Usage:
        mx-scaler-util [Options]...
Options:
        -v, --version
                Get scaler firmware version
        -m, --model
                Get model name
        -l, --lightsensor_level
                Get current light sensor level value
        -s, --lightsensor [0~2]
                Set light sensor mode, query without arg
        -b, --brightness [0~9]
                Set brightness, query without arg
        -t, --touch-button [0~3] [on|off]
                Set touch button status status:
                        0: pwr button
                        1: br+ button
                        2: br- button
                        3: all buttons
                        Query all button status without arg
        -p, --touch-panel [0~1]
                Set touch panel on(1)/off(0), query without arg
```

:::caution
Moxa scaler service will occupy the MCU ommunication port,
please stop the scaler service before using the MCU control tools. (e.g. MCU upgrade tool)
:::

For exmaple:
```
# Stop Moxa scaler Service
systemctl stop moxa-scalerd.service

# Check communication port status (if needed)
lsof /dev/ttyS2
```
