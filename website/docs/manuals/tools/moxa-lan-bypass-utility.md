---
title: moxa-lan-bypass-utility
---

### Moxa LAN Bypass Utility

The `Moxa LAN Bypass Utility` is a MCU utility for LAN Bypass status (connect/disconnect/bypass) control.

### Source Code Link
[moxa-lan-bypass-utility](https://gitlab.com/moxa/open-source/linux/packages/moxa-lan-bypass-utility/-/tree/master)

### Support Model

- V Series:
  - [V3000](../products/V/V3000.md): for LAN7 and LAN8

### Build Utility

```
apt update && apt install build-essential
git clone https://gitlab.com/moxa/ibg/software/platform/linux/packages/moxa-lan-bypass-utility.git -b master
cd moxa-lpc-mcu-upgrade-tool
make
make install
```

### Usage

```
Usage:
        mx-lanbypass-ctl [Options]...
Options:
        -r, --relaymode
                Get relay mode (0:connect|1:disconnect|2:by-pass)
        -r, --relaymode [0|1|2]
                Set relay mode (0:connect|1:disconnect|2:by-pass)
        -w, --wdtresetmode
                Get wdt reset mode (0:reset off|1:reset on)
        -w, --wdtresetmode [0|1]
                Set wdt reset mode (0:reset off|1:reset on)
        -m, --wdtrelaymode
                Get wdt relay mode (0:connect|1:disconnect|2:by-pass)
        -m, --wdtrelaymode [0|1|2]
                Set wdt relay mode (0:connect|1:disconnect|2:by-pass)
```

:::caution
Before using lan bypass utility, please stop Moxa APP Watchdog Service to avoid communication conflict issue.
:::

```
# Stop Moxa APP Watchdog Service
systemctl stop mx-app-wdtd.service

# Check communication port status (if needed)
lsof /dev/ttyS2
```
