---
title: moxa-lpc-mcu-upgrade-tool
---

### Moxa LPC MCU Upgrade Tool

The `Moxa LPC MCU Upgrade Tool` is an LPC MCU upgrade utility for EXPC-F2000/V3000 series.

### Source Code Link
[moxa-lpc-mcu-upgrade-tool](https://gitlab.com/moxa/open-source/linux/packages/moxa-lpc-mcu-upgrade-tool/-/tree/master)

### Support Model

- V Series:
  - [V3000](../products/V/V3000.md)

- EXPC Series:
  - [EXPC-F2000](../products/EXPC/EXPC-F2000.md)

### Prerequisite
Before use `moxa-lpc-mcu-upgrade-tool`, you should build source code and install to /sbin folder:

- Build source code

```
apt update && apt install build-essential git
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-lpc-mcu-upgrade-tool.git -b master
cd moxa-lpc-mcu-upgrade-tool
make
make install
```

### Usage

```
Usage:
        mx-lpc-mcu-upgrade-tool [Options]...
Options:
        -f, --file
                Start MCU upgrade from file
        -v, --version
                Get current MCU version
Example:
        mx-lpc-mcu-upgrade-tool -f FB_MCU_V3000_V1.00S03_22060219.bin
        mx-lpc-mcu-upgrade-tool -v
```

:::caution
Before using MCU firmware upgrade tool, please stop Moxa MCU related serivce to avoid communication conflict issue.
:::

- For `EXPC-F2000` series, to stop scaler control daemon service before upgrade MCU
```
systemctl stop mx-scalered.service
```

- For `V3000` series, to stop app wdt deamon serice before upgrade MCU
```
systemctl stop mx-app-wdtd.service
```

- To check MCU uart port is occupied by systemd service
```
losf /dev/ttyS2
```
