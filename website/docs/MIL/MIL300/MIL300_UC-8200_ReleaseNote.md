---
title: UC-8200
---

## Moxa Industrial Linux(MIL) 3.0.0 for UC-8200 Series

| **System Image Version** | Build No. | Debian Ver. | Kernel Ver.                    | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------ | ------------ |
| v1.0                | 22122314 | 11.5        | linux 5.10.131-cip13-rt5-moxa8+deb11 | 1/5/2023    |

## Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log) 
2. [Debian Software Package Change Log](#debian-software-package-change-log)


## Moxa Package Change Log

| Package                                          | Major Change    | Version                                            | 
| ------------------------------------------------ | ------- | -------------------------------------------------- |
|e2fsprogs|First Release| 1.46.2-2+moxa1|
|libcom-err2|First Release| 1.46.2-2+moxa1|
|libext2fs2|First Release| 1.46.2-2+moxa1|
|libmm-glib0|First Release| 1.18.6-2~bp11+moxa2|
|libnm0|First Release| 1.36.2-1~bp11+moxa2|
|libss2|First Release| 1.46.2-2+moxa1|
|linux-headers-5.10.0-cip-rt-moxa-imx7d|First Release| 5.10.131-cip13-rt5-moxa8+deb11|
|linux-image-5.10.0-cip-rt-moxa-imx7d|First Release| 5.10.131-cip13-rt5-moxa8+deb11|
|linux-kbuild-5.10.0-cip-rt-moxa-imx7d|First Release| 5.10.131-cip13-rt5-moxa8+deb11|
|logsave|First Release| 1.46.2-2+moxa1|
|modemmanager|First Release| 1.18.6-2~bp11+moxa2|
|moxa-nport-real-tty-utils|First Release| 5.1+1.0.0+deb11+moxa1|
|network-manager|First Release| 1.36.2-1~bp11+moxa2|
|moxa-archive-keyring|First Release| 2022.09+deb11|
|moxa-bootloader-manager|First Release| 1.2.1+deb11|
|moxa-computer-interface-manager|First Release| 1.3.1+deb11|
|moxa-connection-manager|First Release| 1.2.17+deb11|
|moxa-guardian|First Release| 0.14.0+deb11|
|moxa-image-archive-keyring|First Release| 1.3.0+deb11|
|moxa-mil-base-system-armhf|First Release| 3.0.2+deb11u3|
|moxa-system-manager|First Release| 2.9.0+deb11|
|moxa-telit-firmware-upgrade-tool|First Release| 2.0.7+deb11|
|moxa-uc-8200-base-system|First Release| 3.0.1+deb11u10|
|moxa-version|First Release| 1.3.3+deb11|





## Debian Software Package Change Log

| Package                                  | Major Change    | Version                                                           |
| ---------------------------------------- | ------- | ----------------------------------------------------------------- |
|acl|First Release| 2.2.53-10|
|adduser|First Release|3.118|
|aide|First Release| 0.17.3-4+deb11u1|
|apt|First Release| 2.2.4|
|apt-transport-https|First Release| 2.2.4|
|apt-utils|First Release| 2.2.4|
|auditd|First Release| 1:3.0-2|
|base-files|First Release| 11.1+deb11u5|
|base-passwd|First Release| 3.5.51|
|bash|First Release| 5.1-2+deb11u1|
|bash-completion|First Release| 1:2.11-2|
|bsdextrautils|First Release| 2.36.1-8+deb11u1|
|bsdmainutils|First Release| 12.1.7+nmu3|
|bsdutils|First Release| 1:2.36.1-8+deb11u1|
|ca-certificates|First Release|20210119|
|can-utils|First Release| 2020.11.0-1|
|chrony|First Release| 4.0-8+deb11u2|
|coreutils|First Release| 8.32-4|
|cpulimit|First Release| 2.6-3|
|cracklib-runtime|First Release| 2.9.6-3.4|
|crda|First Release| 4.14+git20191112.9856751-1|
|curl|First Release| 7.74.0-1.3+deb11u3|
|dash|First Release| 0.5.11+git20200708+dd9ef66-5|
|dbus|First Release| 1.12.24-0+deb11u1|
|debconf|First Release| 1.5.77|
|debian-archive-keyring|First Release| 2021.1.1|
|debianutils|First Release| 4.11.2|
|debsums|First Release| 3.0.2|
|dialog|First Release| 1.3-20201126-1|
|diffutils|First Release| 1:3.7-5|
|dirmngr|First Release| 2.2.27-2+deb11u2|
|dmsetup|First Release| 2:1.02.175-2.1|
|dnsmasq-base|First Release| 2.85-1|
|dpkg|First Release| 1.20.12|
|fail2ban|First Release| 0.11.2-2|
|fdisk|First Release| 2.36.1-8+deb11u1|
|file|First Release| 1:5.39-3|
|findutils|First Release| 4.8.0-1|
|firmware-atheros|First Release| 20210315-3|
|firmware-misc-nonfree|First Release| 20210315-3|
|gcc-10-base|First Release| 10.2.1-6|
|gcc-9-base|First Release| 9.3.0-22|
|gnupg|First Release| 2.2.27-2+deb11u2|
|gnupg-l10n|First Release| 2.2.27-2+deb11u2|
|gnupg-utils|First Release| 2.2.27-2+deb11u2|
|gpg|First Release| 2.2.27-2+deb11u2|
|gpg-agent|First Release| 2.2.27-2+deb11u2|
|gpg-wks-client|First Release| 2.2.27-2+deb11u2|
|gpg-wks-server|First Release| 2.2.27-2+deb11u2|
|gpgconf|First Release| 2.2.27-2+deb11u2|
|gpgsm|First Release| 2.2.27-2+deb11u2|
|gpgv|First Release| 2.2.27-2+deb11u2|
|grep|First Release| 3.6-1|
|gzip|First Release| 1.10-4+deb11u1|
|hostname|First Release|3.23|
|ifupdown|First Release| 0.8.36|
|init|First Release|1.6|
|init-system-helpers|First Release|1.6|
|iproute2|First Release| 5.10.0-4|
|iputils-ping|First Release| 3:20210202-1|
|isc-dhcp-client|First Release| 4.4.1-2.3+deb11u1|
|iw|First Release| 5.9-3|
|jq|First Release| 1.6-2.1|
|kmod|First Release| 28-1|
|libacl1|First Release| 2.2.53-10|
|libapparmor1|First Release| 2.13.6-10|
|libapt-pkg6.0|First Release| 2.2.4|
|libargon2-1|First Release| 0~20171227-0.2|
|libassuan0|First Release| 2.5.3-7.1|
|libattr1|First Release| 1:2.4.48-6|
|libaudit-common|First Release| 1:3.0-2|
|libaudit1|First Release| 1:3.0-2|
|libauparse0|First Release| 1:3.0-2|
|libblkid1|First Release| 2.36.1-8+deb11u1|
|libbluetooth3|First Release| 5.55-3.1|
|libbpf0|First Release| 1:0.3-2|
|libbrotli1|First Release| 1.0.9-2+b2|
|libbsd0|First Release| 0.11.3-1|
|libbz2-1.0|First Release| 1.0.8-4|
|libc-bin|First Release| 2.31-13+deb11u4|
|libc-l10n|First Release| 2.31-13+deb11u4|
|libc6|First Release| 2.31-13+deb11u4|
|libcap-ng0|First Release| 0.7.9-2.2+b1|
|libcap2|First Release| 1:2.44-1|
|libcap2-bin|First Release| 1:2.44-1|
|libcbor0|First Release| 0.5.0+dfsg-2|
|libcrack2|First Release| 2.9.6-3.4|
|libcrypt1|First Release| 1:4.4.18-4|
|libcryptsetup12|First Release| 2:2.3.7-1+deb11u1|
|libcurl3-gnutls|First Release| 7.74.0-1.3+deb11u3|
|libcurl4|First Release| 7.74.0-1.3+deb11u3|
|libdb5.3|First Release| 5.3.28+dfsg1-0.8|
|libdbus-1-3|First Release| 1.12.24-0+deb11u1|
|libdebconfclient0|First Release|0.26|
|libdevmapper1.02.1|First Release| 2:1.02.175-2.1|
|libdns-export1110|First Release| 1:9.11.19+dfsg-2.1|
|libdpkg-perl|First Release| 1.20.12|
|libedit2|First Release| 3.1-20191231-2+b1|
|libelf1|First Release| 0.183-1|
|libestr0|First Release| 0.1.10-2.1+b1|
|libexpat1|First Release| 2.2.10-2+deb11u5|
|libfastjson4|First Release| 0.99.9-1|
|libfdisk1|First Release| 2.36.1-8+deb11u1|
|libffi7|First Release| 3.3-6|
|libfido2-1|First Release| 1.6.0-2|
|libfile-fnmatch-perl|First Release| 0.02-2+b8|
|libgcc-s1|First Release| 10.2.1-6|
|libgcrypt20|First Release| 1.8.7-6|
|libgdbm-compat4|First Release| 1.19-2|
|libgdbm6|First Release| 1.19-2|
|libglib2.0-0|First Release| 2.66.8-1|
|libgmp10|First Release| 2:6.2.1+dfsg-1+deb11u1|
|libgnutls30|First Release| 3.7.1-5+deb11u2|
|libgpg-error0|First Release| 1.38-2|
|libgpm2|First Release| 1.20.7-8|
|libgssapi-krb5-2|First Release| 1.18.3-6+deb11u2|
|libgudev-1.0-0|First Release| 234-1|
|libhogweed6|First Release| 3.7.3-1|
|libidn2-0|First Release| 2.3.0-5|
|libiniparser1|First Release| 4.1-4|
|libip4tc2|First Release| 1.8.7-1|
|libisc-export1105|First Release| 1:9.11.19+dfsg-2.1|
|libjansson4|First Release| 2.13.1-1.1|
|libjq1|First Release| 1.6-2.1|
|libjson-c5|First Release| 0.15-2|
|libk5crypto3|First Release| 1.18.3-6+deb11u2|
|libkeyutils1|First Release| 1.6.1-2|
|libkmod2|First Release| 28-1|
|libkrb5-3|First Release| 1.18.3-6+deb11u2|
|libkrb5support0|First Release| 1.18.3-6+deb11u2|
|libksba8|First Release| 1.5.0-3+deb11u1|
|libldap-2.4-2|First Release| 2.4.57+dfsg-3+deb11u1|
|liblognorm5|First Release| 2.0.5-1.1|
|liblz4-1|First Release| 1.9.3-2|
|liblzma5|First Release| 5.2.5-2.1~deb11u1|
|liblzo2-2|First Release| 2.10-2|
|libmagic-mgc|First Release| 1:5.39-3|
|libmagic1|First Release| 1:5.39-3|
|libmaxminddb0|First Release| 1.5.2-1|
|libmbim-glib4|First Release| 1.26.2-1~bpo11+1|
|libmbim-proxy|First Release| 1.26.2-1~bpo11+1|
|libmcm0|First Release| 1.2.17+deb11|
|libmd0|First Release| 1.0.3-3|
|libmnl0|First Release| 1.0.4-3|
|libmount1|First Release| 2.36.1-8+deb11u1|
|libmpdec3|First Release| 2.5.1-1|
|libmxio|First Release| 2.12.5+21031016+deb11|
|libmxio-dev|First Release| 2.12.5+21031016+deb11|
|libncurses6|First Release| 6.2+20201114-2|
|libncursesw6|First Release| 6.2+20201114-2|
|libndp0|First Release| 1.6-1+b1|
|libnetfilter-conntrack3|First Release| 1.0.8-3|
|libnettle8|First Release| 3.7.3-1|
|libnewt0.52|First Release| 0.52.21-4+b3|
|libnfnetlink0|First Release| 1.0.1-3+b1|
|libnftables1|First Release| 0.9.8-3.1|
|libnftnl11|First Release| 1.1.9-1|
|libnghttp2-14|First Release| 1.43.0-1|
|libnl-3-200|First Release| 3.4.0-1+b1|
|libnl-genl-3-200|First Release| 3.4.0-1+b1|
|libnl-route-3-200|First Release| 3.4.0-1+b1|
|libnpth0|First Release| 1.6-3|
|libnsl2|First Release| 1.3.0-2|
|libonig5|First Release| 6.9.6-1.1|
|libp11-kit0|First Release| 0.23.22-1|
|libpam-modules|First Release| 1.4.0-9+deb11u1|
|libpam-modules-bin|First Release| 1.4.0-9+deb11u1|
|libpam-pwquality|First Release| 1.4.4-1|
|libpam-runtime|First Release| 1.4.0-9+deb11u1|
|libpam-systemd|First Release| 247.3-7+deb11u1|
|libpam0g|First Release| 1.4.0-9+deb11u1|
|libparted2|First Release| 3.4-1|
|libpcap0.8|First Release| 1.10.0-2|
|libpcre2-8-0|First Release| 10.36-2+deb11u1|
|libpcre3|First Release| 2:8.39-13|
|libpcsclite1|First Release| 1.9.1-1|
|libperl5.32|First Release| 5.32.1-4+deb11u2|
|libpolkit-agent-1-0|First Release| 0.105-31+deb11u1|
|libpolkit-gobject-1-0|First Release| 0.105-31+deb11u1|
|libpopt0|First Release| 1.18-2|
|libprocps8|First Release| 2:3.3.17-5|
|libpsl5|First Release| 0.21.0-1.2|
|libpwquality-common|First Release| 1.4.4-1|
|libpwquality1|First Release| 1.4.4-1|
|libpython3-stdlib|First Release| 3.9.2-3|
|libpython3.9-minimal|First Release| 3.9.2-1|
|libpython3.9-stdlib|First Release| 3.9.2-1|
|libqmi-glib5|First Release| 1.30.4-1~bpo11+1|
|libqmi-proxy|First Release| 1.30.4-1~bpo11+1|
|libreadline8|First Release| 8.1-1|
|librtmp1|First Release| 2.4+20151223.gitfa8646d.1-2+b2|
|libsasl2-2|First Release| 2.1.27+dfsg-2.1+deb11u1|
|libsasl2-modules-db|First Release| 2.1.27+dfsg-2.1+deb11u1|
|libseccomp2|First Release| 2.5.1-1+deb11u1|
|libselinux1|First Release| 3.1-3|
|libsemanage-common|First Release| 3.1-1|
|libsemanage1|First Release| 3.1-1+b2|
|libsepol1|First Release| 3.1-1|
|libslang2|First Release| 2.3.2-5|
|libsmartcols1|First Release| 2.36.1-8+deb11u1|
|libsqlite3-0|First Release| 3.34.1-3|
|libssh2-1|First Release| 1.9.0-2|
|libssl1.1|First Release| 1.1.1n-0+deb11u3|
|libstdc++6|First Release| 10.2.1-6|
|libsystemd0|First Release| 247.3-7+deb11u1|
|libtasn1-6|First Release| 4.16.0-2|
|libteamdctl0|First Release| 1.31-1|
|libtinfo6|First Release| 6.2+20201114-2|
|libtirpc-common|First Release| 1.3.1-1+deb11u1|
|libtirpc3|First Release| 1.3.1-1+deb11u1|
|libtss2-esys-3.0.2-0|First Release| 3.0.3-2|
|libtss2-mu0|First Release| 3.0.3-2|
|libtss2-rc0|First Release| 3.0.3-2|
|libtss2-sys1|First Release| 3.0.3-2|
|libtss2-tcti-cmd0|First Release| 3.0.3-2|
|libtss2-tcti-device0|First Release| 3.0.3-2|
|libtss2-tcti-mssim0|First Release| 3.0.3-2|
|libtss2-tcti-swtpm0|First Release| 3.0.3-2|
|libtss2-tctildr0|First Release| 3.0.3-2|
|libubootenv-tool|First Release| 0.3.2-0.1|
|libubootenv0.1|First Release| 0.3.2-0.1|
|libudev1|First Release| 247.3-7+deb11u1|
|libunistring2|First Release| 0.9.10-4|
|libuuid1|First Release| 2.36.1-8+deb11u1|
|libwrap0|First Release| 7.6.q-31|
|libxtables12|First Release| 1.8.7-1|
|libxxhash0|First Release| 0.8.0-2|
|libzstd1|First Release| 1.4.8+dfsg-2.1|
|locales|First Release| 2.31-13+deb11u4|
|login|First Release| 1:4.8.1-1|
|logrotate|First Release| 3.18.0-2+deb11u1|
|lsb-base|First Release| 11.1.0|
|mawk|First Release| 1.3.4.20200120-2|
|media-types|First Release| 4.0.0|
|mount|First Release| 2.36.1-8+deb11u1|
|mtd-utils|First Release| 1:2.1.2-2|
|ncal|First Release| 12.1.7+nmu3|
|ncurses-base|First Release| 6.2+20201114-2|
|ncurses-bin|First Release| 6.2+20201114-2|
|net-tools|First Release| 1.60+git20181103.0eebece-1|
|netbase|First Release|6.3|
|nftables|First Release| 0.9.8-3.1|
|openssh-client|First Release| 1:8.4p1-5+deb11u1|
|openssh-server|First Release| 1:8.4p1-5+deb11u1|
|openssh-sftp-server|First Release| 1:8.4p1-5+deb11u1|
|openssl|First Release| 1.1.1n-0+deb11u3|
|parted|First Release| 3.4-1|
|passwd|First Release| 1:4.8.1-1|
|perl|First Release| 5.32.1-4+deb11u2|
|perl-base|First Release| 5.32.1-4+deb11u2|
|perl-modules-5.32|First Release| 5.32.1-4+deb11u2|
|pinentry-curses|First Release| 1.1.0-4|
|policykit-1|First Release| 0.105-31+deb11u1|
|procps|First Release| 2:3.3.17-5|
|pv|First Release| 1.6.6-1|
|python3|First Release| 3.9.2-3|
|python3-minimal|First Release| 3.9.2-3|
|python3.9|First Release| 3.9.2-1|
|python3.9-minimal|First Release| 3.9.2-1|
|readline-common|First Release| 8.1-1|
|rsync|First Release| 3.2.3-4+deb11u1|
|rsyslog|First Release| 8.2102.0-2+deb11u1|
|runit-helper|First Release| 2.10.3|
|sed|First Release| 4.7-1|
|sensible-utils|First Release| 0.0.14|
|sudo|First Release| 1.9.5p2-3|
|systemd|First Release| 247.3-7+deb11u1|
|systemd-sysv|First Release| 247.3-7+deb11u1|
|sysvinit-utils|First Release| 2.96-7+deb11u1|
|tar|First Release| 1.34+dfsg-1|
|tpm-udev|First Release|0.5|
|tpm2-tools|First Release| 5.0-2|
|tzdata|First Release| 2021a-1+deb11u5|
|u-boot-tools|First Release| 2021.01+dfsg-5|
|ucf|First Release|3.0043|
|udev|First Release| 247.3-7+deb11u1|
|util-linux|First Release| 2.36.1-8+deb11u1|
|vim|First Release| 2:8.2.2434-3+deb11u1|
|vim-common|First Release| 2:8.2.2434-3+deb11u1|
|vim-runtime|First Release| 2:8.2.2434-3+deb11u1|
|vnstat|First Release| 2.6-3|
|watchdog|First Release| 5.16-1|
|wget|First Release| 1.21-1+deb11u1|
|whiptail|First Release| 0.52.21-4+b3|
|wireless-regdb|First Release| 2022.04.08-2~deb11u1|
|wpasupplicant|First Release| 2:2.9.0-21|
|xxd|First Release| 2:8.2.2434-3+deb11u1|
|zeek-core|First Release| 4.2.1+deb11|
|zeekctl|First Release| 4.2.1+deb11|
|zlib1g|First Release| 1:1.2.11.dfsg-2+deb11u2|





