---
title: V2406C-WL
---
## Moxa Industrial Linux(MIL) 2.1.1 for V2406C-WL Series

| **System Image Version** | Build No. | Debian Ver. | Kernel Ver.                    | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------ | ------------ |
| v1.2                | 22122714 | 10.13        | linux 4.19.208-1+4.1-moxa1+deb10u1 | 12/29/2022    |

## Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log)
2. [Debian Software Package Change Log](#debian-software-package-change-log)


## Moxa Package Change Log

| Package                                          | Major Change    | Version                                            |                                                                                               
| ------------------------------------------------ | ------- | -------------------------------------------------- | 
|linux-image-4.19.0-amd64-moxa-whiskeylake|First Release| 4.19.249-2-moxa1+deb10|
|moxa-archive-keyring|First Release| 2022.09+deb10|
|moxa-atheros-ath10k-driver-amd64|First Release| 4.19.171+1.0.0+deb10u2|
|moxa-audio-retask|First Release| 1.0.0+deb10|
|moxa-bios-upgrade-tool|First Release| 1.0.0|
|moxa-computer-interface-manager|First Release| 1.3.1+deb10|
|moxa-connection-manager|First Release| 1.2.18+deb10|
|moxa-hotswap-driver-amd64|First Release| 4.19.208-1+1.0.0+deb10u1|
|moxa-hwmon-ina2xx-driver-amd64|First Release| 4.19.171+1.0.0+deb10u2|
|moxa-it87-gpio-driver-amd64|First Release| 5.2+1.3.0+deb10u2|
|moxa-it87-serial-driver-amd64|First Release| 1.4.1+deb10|
|moxa-it87-wdt-driver-amd64|First Release| 5.2+1.3.0+deb10u2|
|moxa-mil-base-system-amd64|First Release| 1.1.6+deb10u2|
|moxa-mxu11x0-driver-amd64|First Release| 4.19.208-1+4.1+deb10u1|
|moxa-mxuport-driver-amd64|First Release| 4.19.208-1+4.1-moxa1+deb10u1|
|moxa-nport-real-tty-driver-amd64|First Release| 4.19.208-1+4.1+deb10u1|
|moxa-nport-real-tty-utils|First Release| 4.1+1.0.0+deb10|
|moxa-qmi-wwan-driver-amd64|First Release| 4.19.171+1.0.0+deb10u2|
|moxa-system-manager|First Release| 2.5.1+deb10|
|moxa-telit-firmware-upgrade-tool|First Release| 2.0.7+deb10|
|moxa-v2406c-base-system|First Release| 1.2.0+deb10|
|moxa-v2406c-hotswapd|First Release| 1.0.1+deb10|
|moxa-version|First Release| 1.3.3+deb10|
|libmm-glib0|First Release| 1.10.0-1-moxa4|
|libnm0|First Release| 1.14.6-2+deb10u1-moxa1|
|libtss2-esys-3.0.2-0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-mu0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-rc0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-sys1|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-tcti-cmd0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-tcti-device0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-tcti-mssim0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-tcti-swtpm0|First Release| 3.0.3-2-moxa1+deb10|
|libtss2-tctildr0|First Release| 3.0.3-2-moxa1+deb10|
|network-manager|First Release| 1.14.6-2+deb10u1-moxa1|
|tpm-udev|First Release| 0.5-moxa1+deb10|
|tpm2-tools|First Release| 5.0-2-moxa1+deb10|
|vnstat|First Release| 2.6-3-moxa1|



## Debian Software Package Change Log

| Package                                  | Major Change    | Version                                                           |
| ---------------------------------------- | ------- | ----------------------------------------------------------------- |
|adduser|First Release|3.118|
|alsa-tools|First Release| 1.1.7-1|
|apt|First Release| 1.8.2.3|
|apt-transport-https|First Release| 1.8.2.3|
|apt-utils|First Release| 1.8.2.3|
|base-files|First Release| 10.3+deb10u13|
|base-passwd|First Release| 3.5.46|
|bash|First Release| 5.0-4|
|bash-completion|First Release| 1:2.8-6|
|binutils|First Release| 2.31.1-16|
|binutils-common|First Release| 2.31.1-16|
|binutils-x86-64-linux-gnu|First Release| 2.31.1-16|
|bluez|First Release| 5.50-1.2~deb10u2|
|bsdmainutils|First Release| 11.1.2+b1|
|bsdutils|First Release| 1:2.33.1-0.1|
|build-essential|First Release|12.6|
|busybox|First Release| 1:1.30.1-4|
|bzip2|First Release| 1.0.6-9.2~deb10u1|
|ca-certificates|First Release| 20200601~deb10u2|
|coreutils|First Release| 8.30-3|
|cpp|First Release| 4:8.3.0-1|
|cpp-8|First Release| 8.3.0-6|
|cracklib-runtime|First Release| 2.9.6-2|
|crda|First Release| 3.18-1|
|curl|First Release| 7.64.0-4+deb10u3|
|dash|First Release| 0.5.10.2-5|
|dbus|First Release| 1.12.20-0+deb10u1|
|debconf|First Release| 1.5.71+deb10u1|
|debian-archive-keyring|First Release| 2019.1+deb10u1|
|debianutils|First Release| 4.8.6.1|
|dialog|First Release| 1.3-20190211-1|
|diffutils|First Release| 1:3.7-3|
|dirmngr|First Release| 2.2.12-1+deb10u2|
|dmidecode|First Release| 3.2-1|
|dmsetup|First Release| 2:1.02.155-3|
|dnsmasq-base|First Release| 2.80-1+deb10u1|
|dpkg|First Release| 1.19.8|
|dpkg-dev|First Release| 1.19.8|
|e2fsprogs|First Release| 1.44.5-1+deb10u3|
|efivar|First Release| 37-2+deb10u1|
|fakeroot|First Release| 1.23-1|
|fdisk|First Release| 2.33.1-0.1|
|file|First Release| 1:5.35-4+deb10u2|
|findutils|First Release| 4.6.0+git+20190209-2|
|firmware-atheros|First Release| 20190114-2|
|firmware-misc-nonfree|First Release| 20190114-2|
|g++|First Release| 4:8.3.0-1|
|g++-8|First Release| 8.3.0-6|
|gcc|First Release| 4:8.3.0-1|
|gcc-8|First Release| 8.3.0-6|
|gcc-8-base|First Release| 8.3.0-6|
|gnupg|First Release| 2.2.12-1+deb10u2|
|gnupg-l10n|First Release| 2.2.12-1+deb10u2|
|gnupg-utils|First Release| 2.2.12-1+deb10u2|
|gpg|First Release| 2.2.12-1+deb10u2|
|gpg-agent|First Release| 2.2.12-1+deb10u2|
|gpg-wks-client|First Release| 2.2.12-1+deb10u2|
|gpg-wks-server|First Release| 2.2.12-1+deb10u2|
|gpgconf|First Release| 2.2.12-1+deb10u2|
|gpgsm|First Release| 2.2.12-1+deb10u2|
|gpgv|First Release| 2.2.12-1+deb10u2|
|grep|First Release| 3.3-1|
|groff-base|First Release| 1.22.4-3+deb10u1|
|gzip|First Release| 1.9-3+deb10u1|
|hostname|First Release|3.21|
|i2c-tools|First Release| 4.1-1|
|ifupdown|First Release| 0.8.35|
|init-system-helpers|First Release| 1.56+nmu1|
|insyde-phy-alloc-driver-amd64|First Release| 4.19.208-1+8+deb10u2|
|iproute2|First Release| 4.20.0-2+deb10u1|
|iptables|First Release| 1.8.2-4|
|iputils-ping|First Release| 3:20180629-2+deb10u2|
|isc-dhcp-client|First Release| 4.4.1-2+deb10u1|
|isc-dhcp-common|First Release| 4.4.1-2+deb10u1|
|iw|First Release| 5.0.1-1|
|jq|First Release| 1.5+dfsg-2+b1|
|kmod|First Release| 26-1|
|krb5-locales|First Release| 1.17-3+deb10u4|
|libacl1|First Release| 2.2.53-4|
|libalgorithm-diff-perl|First Release| 1.19.03-2|
|libalgorithm-diff-xs-perl|First Release| 0.04-5+b1|
|libalgorithm-merge-perl|First Release| 0.08-3|
|libapparmor1|First Release| 2.13.2-10|
|libapt-inst2.0|First Release| 1.8.2.3|
|libapt-pkg5.0|First Release| 1.8.2.3|
|libargon2-1|First Release| 0~20171227-0.2|
|libasan5|First Release| 8.3.0-6|
|libasound2|First Release| 1.1.8-1|
|libasound2-data|First Release| 1.1.8-1|
|libassuan0|First Release| 2.5.2-1|
|libatm1|First Release| 1:2.5.1-2|
|libatomic1|First Release| 8.3.0-6|
|libattr1|First Release| 1:2.4.48-4|
|libaudit-common|First Release| 1:2.8.4-3|
|libaudit1|First Release| 1:2.8.4-3|
|libbinutils|First Release| 2.31.1-16|
|libblkid1|First Release| 2.33.1-0.1|
|libbluetooth3|First Release| 5.50-1.2~deb10u2|
|libbsd0|First Release| 0.9.1-2+deb10u1|
|libbz2-1.0|First Release| 1.0.6-9.2~deb10u1|
|libc-bin|First Release| 2.28-10+deb10u1|
|libc-dev-bin|First Release| 2.28-10+deb10u1|
|libc-l10n|First Release| 2.28-10+deb10u1|
|libc6|First Release| 2.28-10+deb10u1|
|libc6-dev|First Release| 2.28-10+deb10u1|
|libcap-ng0|First Release| 0.7.9-2|
|libcap2|First Release| 1:2.25-2|
|libcap2-bin|First Release| 1:2.25-2|
|libcc1-0|First Release| 8.3.0-6|
|libcom-err2|First Release| 1.44.5-1+deb10u3|
|libcrack2|First Release| 2.9.6-2|
|libcryptsetup12|First Release| 2:2.1.0-5+deb10u2|
|libcurl3-gnutls|First Release| 7.64.0-4+deb10u3|
|libcurl4|First Release| 7.64.0-4+deb10u3|
|libdb5.3|First Release 5.3.28+dfsg1-0.5|
|libdbus-1-3|First Release 1.12.20-0+deb10u1|
|libdebconfclient0|First Release0.249|
|libdevmapper1.02.1|First Release| 2:1.02.155-3|
|libdns-export1104|First Release| 1:9.11.5.P4+dfsg-5.1+deb10u7|
|libdpkg-perl|First Release| 1.19.8|
|libdw1|First Release| 0.176-1.1|
|libedit2|First Release| 3.1-20181209-1|
|libefiboot1|First Release| 37-2+deb10u1|
|libefivar1|First Release| 37-2+deb10u1|
|libelf1|First Release| 0.176-1.1|
|libestr0|First Release| 0.1.10-2.1|
|libexpat1|First Release| 2.2.6-2+deb10u4|
|libext2fs2|First Release| 1.44.5-1+deb10u3|
|libfakeroot|First Release| 1.23-1|
|libfastjson4|First Release| 0.99.8-2|
|libfdisk1|First Release| 2.33.1-0.1|
|libffi6|First Release| 3.2.1-9|
|libfile-fcntllock-perl|First Release| 0.22-3+b5|
|libgcc-8-dev|First Release| 8.3.0-6|
|libgcc1|First Release| 1:8.3.0-6|
|libgcrypt20|First Release| 1.8.4-5+deb10u1|
|libgdbm-compat4|First Release| 1.18.1-4|
|libgdbm6|First Release| 1.18.1-4|
|libglib2.0-0|First Release| 2.58.3-2+deb10u3|
|libgmp10|First Release| 2:6.1.2+dfsg-4+deb10u1|
|libgnutls30|First Release| 3.6.7-4+deb10u9|
|libgomp1|First Release| 8.3.0-6|
|libgpg-error0|First Release| 1.35-1|
|libgpm2|First Release| 1.20.7-5|
|libgssapi-krb5-2|First Release| 1.17-3+deb10u4|
|libgudev-1.0-0|First Release| 232-2|
|libhogweed4|First Release| 3.4.1-1+deb10u1|
|libi2c0|First Release| 4.1-1|
|libidn11|First Release| 1.33-2.2|
|libidn2-0|First Release| 2.0.5-1+deb10u1|
|libip4tc0|First Release| 1.8.2-4|
|libip6tc0|First Release| 1.8.2-4|
|libiptc0|First Release| 1.8.2-4|
|libisc-export1100|First Release| 1:9.11.5.P4+dfsg-5.1+deb10u7|
|libisl19|First Release| 0.20-2|
|libitm1|First Release| 8.3.0-6|
|libiw30|First Release| 30~pre9-13|
|libjansson4|First Release| 2.12-1|
|libjq1|First Release| 1.5+dfsg-2+b1|
|libjson-c3|First Release| 0.12.1+ds-2+deb10u1|
|libk5crypto3|First Release| 1.17-3+deb10u4|
|libkeyutils1|First Release| 1.6-6|
|libkmod2|First Release| 26-1|
|libkrb5-3|First Release| 1.17-3+deb10u4|
|libkrb5support0|First Release| 1.17-3+deb10u4|
|libksba8|First Release| 1.3.5-2|
|libldap-2.4-2|First Release| 2.4.47+dfsg-3+deb10u7|
|libldap-common|First Release| 2.4.47+dfsg-3+deb10u7|
|liblocale-gettext-perl|First Release| 1.07-3+b4|
|liblognorm5|First Release| 2.0.5-1|
|liblsan0|First Release| 8.3.0-6|
|liblz4-1|First Release| 1.8.3-1+deb10u1|
|liblzma5|First Release| 5.2.4-1+deb10u1|
|libmagic-mgc|First Release| 1:5.35-4+deb10u2|
|libmagic1|First Release| 1:5.35-4+deb10u2|
|libmbim-glib4|First Release| 1.18.0-1|
|libmbim-proxy|First Release| 1.18.0-1|
|libmcm0|First Release| 1.2.18+deb10|
|watchdog|First Release| 5.15-2|
|wget|First Release| 1.20.1-1.1|
|whiptail|First Release| 0.52.20-8|
|wireless-regdb|First Release| 2016.06.10-1|
|wireless-tools|First Release| 30~pre9-13|
|wpasupplicant|First Release| 2:2.7+git20190128+0c1e29f-6+deb10u3|
|xauth|First Release| 1:1.0.10-1|
|xxd|First Release| 2:8.1.0875-5+deb10u2|
|xz-utils|First Release| 5.2.4-1+deb10u1|
|zlib1g|First Release| 1:1.2.11.dfsg-1+deb10u1|

