---
title: UC-3100
---

## MIL 1.1.1 for UC-3100 Series

| **OS Image Version** | Build No. | Debian Ver. | Kernel Ver.                     | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------- | ------------ |
| v1.6                 | 22042718  | 9.13        | linux 4.4.285-cip63-rt36-moxa17 | 6/1/2022    |

### Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log)
2. [Debian Software Package Change Log](#debian-software-package-change-log)


### Moxa Package Change Log

For detail change log of each package, refers to [UC-3100 MIL 1.1.1 Change Log](changelog/MIL111_UC-3100_changelog_220505_155211.zip)

| Package                                | Type    | Version                                             | Major Reason                                                                                                                                                                                                     |
| -------------------------------------- | ------- | --------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|linux-headers-4.4.0-cip-rt-moxa-am335x|Upgrade|'4.4.285-cip63-rt36-moxa13+deb9' to '4.4.285-cip63-rt36-moxa17+deb9'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|linux-kbuild-4.4.0-cip-rt-moxa-am335x|Upgrade|'4.4.285-cip63-rt36-moxa13+deb9' to '4.4.285-cip63-rt36-moxa17+deb9'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|moxa-at-cmd|Upgrade|'0.9.2-1' to '0.9.4-1'|Improve error handling when open device fails|
|moxa-cellular-signald|Upgrade|'2.11.2' to '2.11.4'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|moxa-cellular-utils|Upgrade|'2.11.2' to '2.11.4'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|moxa-mil-base-system-armhf|Upgrade|'1.0.0+deb9' to '1.0.0+deb9u1'|Update MIL version to 1.1.1|
|uc3100-base-system|Upgrade|'1.12.5' to '1.13.0'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|uc3100-kernel|Upgrade|'4.4.285-cip63-rt36-moxa13+deb9' to '4.4.285-cip63-rt36-moxa17+deb9'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|
|uc3100-modules-std|Upgrade|'4.4.285-cip63-rt36-moxa13+deb9' to '4.4.285-cip63-rt36-moxa17+deb9'|Support the new UC-3100 models without bluetooh and Wi-Fi interface (UC-3111-T-EU-LX-NW,C-3111-T-US-LX-NW, UC-3111-T-EU-LX-EU)|


### Debian Software Package Change Log

For detail change log of each package, refers to [UC-3100 MIL 1.1.1 Change Log](changelog/MIL111_UC-3100_changelog_220505_155211.zip)

| Package          | Type    | Version                                                           |
| ---------------- | ------- | ----------------------------------------------------------------- |
|apache2|Upgrade|'2.4.25-3+deb9u11' to '2.4.25-3+deb9u13'|
|apache2-bin|Upgrade|'2.4.25-3+deb9u11' to '2.4.25-3+deb9u13'|
|apache2-data|Upgrade|'2.4.25-3+deb9u11' to '2.4.25-3+deb9u13'|
|apache2-utils|Upgrade|'2.4.25-3+deb9u11' to '2.4.25-3+deb9u13'|
|gzip|Upgrade|'1.6-5+b1' to '1.6-5+deb9u1'|
|libapr1|Upgrade|'1.5.2-5' to '1.5.2-5+deb9u1'|
|libdns-export162|Upgrade|'1:9.10.3.dfsg.P4-12.3+deb9u10' to '1:9.10.3.dfsg.P4-12.3+deb9u12'|
|libexpat1|Upgrade|'2.2.0-2+deb9u3' to '2.2.0-2+deb9u5'|
|libisc-export160|Upgrade|'1:9.10.3.dfsg.P4-12.3+deb9u10' to '1:9.10.3.dfsg.P4-12.3+deb9u12'|
|liblzma5|Upgrade|'5.2.2-1.2+b1' to '5.2.2-1.2+deb9u1'|
|libpolkit-agent-1-0|Upgrade|'0.105-18+deb9u1' to '0.105-18+deb9u2'|
|libpolkit-gobject-1-0|Upgrade|'0.105-18+deb9u1' to '0.105-18+deb9u2'|
|libsasl2-2|Upgrade|'2.1.27\~101-g0780600+dfsg-3+deb9u1' to '2.1.27~101-g0780600+dfsg-3+deb9u2'|
|libsasl2-modules-db|Upgrade|'2.1.27\~101-g0780600+dfsg-3+deb9u1' to '2.1.27~101-g0780600+dfsg-3+deb9u2'|
|libssl1.0.2|Upgrade|'1.0.2u-1\~deb9u6' to '1.0.2u-1~deb9u7'|
|libssl1.1|Upgrade|'1.1.0l-1\~deb9u4' to '1.1.0l-1~deb9u5'|
|libxml2|Upgrade|'2.9.4+dfsg1-2.2+deb9u5' to '2.9.4+dfsg1-2.2+deb9u6'|
|openssl|Upgrade|'1.1.0l-1\~deb9u4' to '1.1.0l-1~deb9u5'|
|tzdata|Upgrade|'2021a-0+deb9u2' to '2021a-0+deb9u3'|
|vim|Upgrade|'2:8.0.0197-4+deb9u3' to '2:8.0.0197-4+deb9u5'|
|vim-common|Upgrade|'2:8.0.0197-4+deb9u3' to '2:8.0.0197-4+deb9u5'|
|vim-runtime|Upgrade|'2:8.0.0197-4+deb9u3' to '2:8.0.0197-4+deb9u5'|
|xxd|Upgrade|'2:8.0.0197-4+deb9u3' to '2:8.0.0197-4+deb9u5'|
|zlib1g|Upgrade|'1:1.2.8.dfsg-5' to '1:1.2.8.dfsg-5+deb9u1'|
|libgcrypt20|Downgrade|'1.8.4-4' to '1.7.6-2+deb9u4'|
|debian-archive-keyring|Upgrade|'2017.5+deb9u1' to '2017.5+deb9u2'|



