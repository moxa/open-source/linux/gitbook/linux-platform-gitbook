---
title: What is Moxa Industrial Linux
slug: /
---

![MIL_logo](img/MIL_logo_2.png)

### What is Moxa Industrial Linux

**Moxa Industrial Linux (MIL)** is a high-performance industrial-grade Linux distribution developed by Moxa to help accelerate your industrial projects. MIL is based on Debian and the standard Linux kernel that makes it easy to deploy applications on multiple systems.  

To address the long-term system needs of smart cities and industries such as power, water, oil & gas, transportation, and factory automation, MIL comes with **10-year Linux support** that includes security patches and bug fixes, making industrial projects secure and sustainable.  

In addition, Moxa is working with industry leaders to create a reliable and secure Linux-based embedded software platform that can be sustained for more than 10 years. Moxa is a member of The Linux Foundation® and is part of its Civil Infrastructure Platform (CIP) project that aims to create an open-source platform for managing and monitoring smart cities, civil infrastructure, and factories to make them secure, reliable, scalable, and sustainable.

### Debian Compatibility
Moxa Industrial Linux is a Debian-based distribution that can use all standard Debian packages for the kernel version. Benefits include:
* Access to the comprehensive repository of packages from Debian
* Field-proven OS stability

### Long-term Support
Moxa Industrial Linux allows users to keep the same kernel version and Debian user space without having to frequently upgrade the entire system. In addition, subscription services for Moxa Industrial Linux, throughout its 10-year life-cycle phase*, provide security updates and bug fixes that include:
*Critical security patches
*High-priority bug fixes

### Robust File System
The robust file system integrated into Moxa Industrial Linux provides extra protection during firmware upgrades and downgrades with:
* Guaranteed system operation when there are power losses during firmware upgrades/downgrades
* Fast and secure reset-to-default function built into the system

### Over-the-air (OTA) Software Updates
Because gateway computers are usually located at remote sites, it is a challenge to upgrade the system and applications. A suitable way to upgrade the firmware on remote computers is to use wireless technology such as cellular or Wi-Fi. Moxa Industrial Linux supports an Advanced Packaging Tools (APT) software-upgrade mechanism for remote upgrades.
